package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="car")
@Table(name="car")
public class Car implements models.Table {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="serial_num")
	private int serialnum;
	
	private String brand;
	private String model;
	private String equipment;
	private Boolean reserved;
	private Boolean selled;
	private String engine;
	private String power;
	private String transmission;
	private String layout;
	
	public Car() {
	}
	
	public Car(int serialnum) {
		this.serialnum = serialnum;
	}
	
	public String getSerialnum() {
		return Integer.toString(serialnum);
	}
	public void setSerialnum(int serialnum) {
		this.serialnum = serialnum;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	public Boolean getReserved() {
		return reserved;
	}
	public void setReserved(Boolean reserved) {
		this.reserved = reserved;
	}
	public String getEngine() {
		return engine;
	}
	public void setEngine(String engine) {
		this.engine = engine;
	}
	public String getPower() {
		return power;
	}
	public void setPower(String power) {
		this.power = power;
	}
	public String getTransmission() {
		return transmission;
	}
	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}
	public String getLayout() {
		return layout;
	}
	public void setLayout(String layout) {
		this.layout = layout;
	}

	public Boolean getSelled() {
		return selled;
	}

	public void setSelled(Boolean selled) {
		this.selled = selled;
	}
	
	@Override
	public String toString() {
		return serialnum + brand + model + equipment + reserved + selled + engine + power + transmission + layout;
	}
}
