package bin;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import models.Table;
import utils.HibernateUtil;

public class SQLManager {
	public List<?> querySelectFromSQL(String queryToTrans) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		List<?> result = null;
		System.out.println(queryToTrans);
	
		try {
			session.beginTransaction();
			
			Query<?> query = session.createQuery(queryToTrans);
			result = query.list();
			
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			System.out.println("beginTransaction failed. " + ex);
		} finally {
			session.close();
		}
		
		for(Object obj: result) {
			System.out.println(obj.toString()); 
		}
		
		return result;
	}
	
	public void queryDeleteFromSQL(String model, String where) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Query<?> query = session.createQuery("delete " + model + " where " + where);
		
		try {
			session.beginTransaction();
			query.executeUpdate();
			session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			System.out.println("beginTransaction failed. " + ex);
		} finally {
			session.close();
		}
	}
	
	public void queryInsertIntoSQL(Table table) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		try {
			session.beginTransaction();
	        session.save(table);
	        session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			System.out.println("beginTransaction failed. " + ex);
		} finally {
			session.close();
		}
	}
	
	public void queryUpdateSQL(String model, String where, String set) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Query<?> query = session.createQuery("update " + model + " set " + set + " where " + where);
		
		try {
			session.beginTransaction();
			query.executeUpdate();
	        session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			System.out.println("beginTransaction failed. " + ex);
		} finally {
			session.close();
		}
	}
	
	public void queryUpdateSQL(Table table) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		try {
			session.beginTransaction();
	        session.merge(table);
	        session.getTransaction().commit();
		} catch (Exception ex) {
			session.getTransaction().rollback();
			System.out.println("beginTransaction failed. " + ex);
		} finally {
			session.close();
		}
	}
}
