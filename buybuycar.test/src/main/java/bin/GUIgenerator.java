package bin;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import models.Car;

public class GUIgenerator {
	SQLManager sqlm = new SQLManager();
	
	Table tableCarList;
	
	Button reserveButton;
	Button sellButton;
	Button addButton;
	Button editButton;
	Button deleteButton;
	
	Text lblSerialNumberText;
	Text lblBrandText;
	Text lblModelText;
	Text lblEquipmentText;
	Text lblEngineText;
	Text lblPowerText;
	Text lblTransmissionText;
	Text lblLayoutText;
	
	Boolean isAddItem = false;
	Boolean isReserved = false;
	Boolean isEditItem = false;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		GUIgenerator gui = new GUIgenerator();
		gui.showRoot();
	}
	
	public void showRoot () {
		Display display = new Display();
		Shell shell = new Shell (display, SWT.SHELL_TRIM & (~SWT.RESIZE));
	    shell.setLayout(new GridLayout(3, false));
		shell.setText("Buy Buy Car");
		shell.setSize(500,400);
		
		//carData layout
		Composite carData = new Composite(shell, SWT.NONE);
		carData.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 3, 1));
		carData.setLayout(new GridLayout(3, false));
		
		//carDataDescription layout
		Composite carDataDescription = new Composite(carData, SWT.BORDER);
		carDataDescription.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
		carDataDescription.setLayout(new GridLayout(2, false));
		
			Label lblSerialNumber = new Label(carDataDescription, SWT.NONE);
			lblSerialNumber.setText("SerialNumber:");
			lblSerialNumberText = new Text(carDataDescription, SWT.SINGLE);
			lblSerialNumberText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
	
			Label lblBrand = new Label(carDataDescription, SWT.NONE);
			lblBrand.setText("Brand:");
			lblBrandText = new Text(carDataDescription, SWT.WRAP);
			lblBrandText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
			
			Label lblModel = new Label(carDataDescription, SWT.NONE);
			lblModel.setText("Model:");
			lblModelText = new Text(carDataDescription, SWT.WRAP);
			lblModelText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
			
			Label lblEquipment = new Label(carDataDescription, SWT.NONE);
			lblEquipment.setText("Equipment:");
			lblEquipmentText = new Text(carDataDescription, SWT.WRAP);
			lblEquipmentText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		
		//carDataSpecifications layout
		Composite carDataSpecifications = new Composite(carData, SWT.BORDER);
		carDataSpecifications.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, false, 1, 1));
		carDataSpecifications.setLayout(new GridLayout(2, false));
		
			Label lblEngine = new Label(carDataSpecifications, SWT.NONE);
			lblEngine.setText("Engine:");
			lblEngineText = new Text(carDataSpecifications, SWT.WRAP);
			lblEngineText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
			
			Label lblPower = new Label(carDataSpecifications, SWT.NONE);
			lblPower.setText("Power:");
			lblPowerText = new Text(carDataSpecifications, SWT.WRAP);
			lblPowerText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
			
			Label lblTransmission = new Label(carDataSpecifications, SWT.NONE);
			lblTransmission.setText("Transmission:");
			lblTransmissionText = new Text(carDataSpecifications, SWT.WRAP);
			lblTransmissionText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
			
			Label lblLayout = new Label(carDataSpecifications, SWT.NONE);
			lblLayout.setText("Layout:");
			lblLayoutText = new Text(carDataSpecifications, SWT.WRAP);
			lblLayoutText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		
		//carDataButtons layout
		Composite carDataButtons = new Composite(carData, SWT.NONE);
		carDataButtons.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true, 1, 1));
		carDataButtons.setLayout(new GridLayout(1, false));
		carDataButtons.setSize(150, SWT.DEFAULT);
		
			//reserver/ok/off button
			reserveButton = new Button(carDataButtons, SWT.PUSH);
			reserveButton.setText("Reserved");
			reserveButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
			reserveButton.addListener(SWT.Selection, new Listener() {
				
				public void handleEvent(Event event) {  
					if(isAddItem) {	
						//add new car model and fill parameters from text fields
						Car car = new Car();
				        car.setSerialnum(Integer.parseInt("-1"));
				        car.setBrand(lblBrandText.getText());
				        car.setModel(lblModelText.getText());
				        car.setEquipment(lblEquipmentText.getText());
				        car.setEngine(lblEngineText.getText());
				        car.setPower(lblPowerText.getText());
				        car.setTransmission(lblTransmissionText.getText());
				        car.setLayout(lblLayoutText.getText());
		
				        //set query: insert into db new car data
				        sqlm.queryInsertIntoSQL(car);
				        
				        //insert into table line with new car data
					    List<?> sel = sqlm.querySelectFromSQL("FROM car");
						TableItem item = new TableItem(tableCarList, SWT.NONE);
						item.setText(0, ((Car) sel.get(sel.size()-1)).getSerialnum());
						item.setText(1, lblBrandText.getText());
						item.setText(2, lblModelText.getText());
						item.setText(3, lblEquipmentText.getText());
						item.setText(4, lblEngineText.getText());
						item.setText(5, lblPowerText.getText());
						item.setText(6, lblTransmissionText.getText());
						item.setText(7, lblLayoutText.getText());
						tableCarList.layout();
						
						//disable editable text fields in car data
						changeEditableTextFields(false);
						
						isAddItem = false;
						
					} else if(isEditItem) {
						//read data from the selected row in the table 
						TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
						
						//add new car model and fill parameters from selected row in the table 
						Car car = new Car();
				        car.setSerialnum(Integer.parseInt("-1"));
				        car.setBrand(lblBrandText.getText());
				        car.setModel(lblModelText.getText());
				        car.setEquipment(lblEquipmentText.getText());
				        car.setEngine(lblEngineText.getText());
				        car.setPower(lblPowerText.getText());
				        car.setTransmission(lblTransmissionText.getText());
				        car.setLayout(lblLayoutText.getText());
						
				        //set query: update entry in db
				        sqlm.queryUpdateSQL(car);
				        
				        //update table line with edited car data
				        selection.setText(0, lblSerialNumberText.getText());
				        selection.setText(1, lblBrandText.getText());
				        selection.setText(2, lblModelText.getText());
				        selection.setText(3, lblEquipmentText.getText());
				        selection.setText(4, lblEngineText.getText());
				        selection.setText(5, lblPowerText.getText());
				        selection.setText(6, lblTransmissionText.getText());
				        selection.setText(7, lblLayoutText.getText());
						
				        //disable editable text fields in car data
				        changeEditableTextFields(false);
				        
						isEditItem = false;
						
					} else if (!isReserved) {
						//read data from the selected row in the table 
						TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
						
						//set query: update column in db 
						sqlm.queryUpdateSQL(Car.class.getName(), "serial_num = " + selection.getText(), "reserver = 1");
						
						//update column in table
						selection.setText(8, "Yes");
						
						//change button name
						reserveButton.setText("Off");
						
					} else if (isReserved) {
						//read data from the selected row in the table 
						TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
						
						//set query: update column in db 
						sqlm.queryUpdateSQL(Car.class.getName(), "serial_num = " + selection.getText(), "reserver = 0");
						
						//update column in table
						selection.setText(8, "No");
						
						//change button name
						reserveButton.setText("Reserved");
					}
					
					//for the end return default name for used buttons
					reserveButton.setText("Reserved");
					sellButton.setText("Sell");
				}
			});
			
			//sell/cancel button
			sellButton = new Button(carDataButtons, SWT.PUSH);
			sellButton.setText("Sell");
			sellButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			sellButton.addListener(SWT.Selection, new Listener() {
				
				public void handleEvent(Event event) {   
					if(isAddItem || isEditItem) {
						//for the end return default name for used buttons
						reserveButton.setText("Reserved");
						sellButton.setText("Sell");
						
						if(isAddItem) {
							//clear text field
							clearTextFields();
						}
						//disable editable text fields in car data
						changeEditableTextFields(false);
						
						isAddItem = false;
						isEditItem = false;
					} else {
						//read data from the selected row in the table 
						TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
						
						//set query: update column in db 
						sqlm.queryUpdateSQL(Car.class.getName(), "serial_num = " + selection.getText(), "selled = 1");
						
						//update column in table
						selection.setText(9, "Yes");;
					}
				}
			});
		
	    Composite carList = new Composite(shell, SWT.BORDER);
	    carList.setLayout(new GridLayout(1, true));
	    carList.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1));
		
	    	//table with cars data
			tableCarList = new Table(carList, SWT.FULL_SELECTION);
			tableCarList.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true, true, 1, 1));
			tableCarList.setLinesVisible(true);
			tableCarList.setHeaderVisible(true);
			changeEditableTextFields(false);
		    
		    String[] titles = {"SerialNum", "Brand", "Model", "Equipment", "Engine", "Power", "Transmission", "Layout", "Reserved", "Selled"};
		    
		    for (int i = 0; i < titles.length; i++) {
		        TableColumn column = new TableColumn(tableCarList, SWT.NONE);
		        column.setText(titles[i]);
		     }
		    
		    //add cars data in table from db
		    for (Object obj: sqlm.querySelectFromSQL("FROM car")) {
		    	Car car = (Car) obj;
				TableItem item = new TableItem(tableCarList, SWT.NONE);
				if(car.getSerialnum() != null) {item.setText(0, car.getSerialnum());}
				if(car.getBrand() != null) {item.setText(1, car.getBrand());}
				if(car.getModel() != null) {item.setText(2, car.getModel());}
				if(car.getEquipment() != null) {item.setText(3, car.getEquipment());}
				if(car.getEngine() != null) {item.setText(4, car.getEngine());}
				if(car.getPower() != null) {item.setText(5, car.getPower());}
				if(car.getTransmission() != null) {item.setText(6, car.getTransmission());}
				if(car.getLayout() != null) {item.setText(7, car.getLayout());}
				if(car.getReserved() != null) {item.setText(8, car.getReserved() ? "Yes" : "No");} else {item.setText(8, "No");}
				if(car.getSelled() != null) {item.setText(9, car.getSelled() ? "Yes" : "No");} else {item.setText(9, "No");}
			}
		    
		    for (int i = 0; i < titles.length; i++) {
		    	tableCarList.getColumn(i).pack();
		    }
		    
		    tableCarList.addListener(SWT.Selection, new Listener() {
		        public void handleEvent(Event e) {
		        	//read data from the selected row in the table 
		        	TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
					
		        	//data from selected row get in text fields with car data
					lblSerialNumberText.setText(selection.getText(0));
					lblBrandText.setText(selection.getText(1));
					lblModelText.setText(selection.getText(2));
					lblEquipmentText.setText(selection.getText(3));
					lblEngineText.setText(selection.getText(4));
					lblPowerText.setText(selection.getText(5));
					lblTransmissionText.setText(selection.getText(6));
					lblLayoutText.setText(selection.getText(7));

					//change button name with availability
					if (selection.getText(8) == "Yes") {
						reserveButton.setText("Off");
						isReserved = true;
					}
					
					//if car is selling
					if (selection.getText(9) == "Yes") {
						//disable editable text fields in car data
						changeEnabledButtons(false);
					} else {
						//enable editable text fields in car data
						changeEnabledButtons(true);
					}
		        }
		    });
	
		    
		Composite carListButton = new Composite(shell, SWT.NONE);
		carListButton.setLayout(new GridLayout(1, true));
		carListButton.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false, true, 1, 1));
		
			addButton = new Button(carListButton, SWT.PUSH);
			addButton.setText("Add");
			addButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			addButton.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event event) {   
					isAddItem = true;
					clearTextFields();
					changeEditableTextFields(true);
					
					reserveButton.setText("Ok");
					sellButton.setText("Cancel");
					
					reserveButton.setEnabled(true);
					sellButton.setEnabled(true);
				}
			});
			
			editButton = new Button(carListButton, SWT.PUSH);
			editButton.setText("Edit");
			editButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			editButton.addListener(SWT.Selection, new Listener() {	
				public void handleEvent(Event event) {   
					isEditItem = true;
					changeEditableTextFields(true);
					
					reserveButton.setText("Ok");
					sellButton.setText("Cancel");
				}
			});
			
			deleteButton = new Button(carListButton, SWT.PUSH);
			deleteButton.setText("Delete");
			deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
			
			deleteButton.addListener(SWT.Selection, new Listener() {
				
				public void handleEvent(Event event) {
					TableItem selection = tableCarList.getItem(tableCarList.getSelectionIndex());
					sqlm.queryDeleteFromSQL(Car.class.getName(), "serial_num = " + selection.getText());
					tableCarList.remove(tableCarList.getSelectionIndices());
					clearTextFields();
				}
			});
			
		shell.open();
		
		while (!shell.isDisposed()) {
		        if (!display.readAndDispatch()) {
		            display.sleep();
		        }
		}

		shell.dispose();
	}
	
	void changeEnabledButtons(Boolean isEnabled) {
		reserveButton.setEnabled(isEnabled);
		sellButton.setEnabled(isEnabled);
		editButton.setEnabled(isEnabled);
		deleteButton.setEnabled(isEnabled);
	}
	
	void changeEditableTextFields(Boolean isEditable) {
		lblSerialNumberText.setEditable(false);
		lblBrandText.setEditable(isEditable);
		lblModelText.setEditable(isEditable);
		lblEquipmentText.setEditable(isEditable);
		lblEngineText.setEditable(isEditable);
		lblPowerText.setEditable(isEditable);
		lblTransmissionText.setEditable(isEditable);
		lblLayoutText.setEditable(isEditable);
	}
	
	void clearTextFields() {
		lblSerialNumberText.setText("");		
		lblBrandText.setText("");	
		lblModelText.setText("");
		lblEquipmentText.setText("");	
		lblEngineText.setText("");
		lblPowerText.setText("");	
		lblTransmissionText.setText("");
		lblLayoutText.setText("");
	}
}
